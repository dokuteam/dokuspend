package doku;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class MainController {

    @RequestMapping("/leaderboard")
    public String greeting() {
        return "leaderboard";
    }

}
